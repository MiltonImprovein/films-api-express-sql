const router = require("express").Router();
const apiUsersRouter = require("./api/users");
const apiFilmsRouter = require("./api/films");
const middlewares = require("./middlewares");

router.use("/users", apiUsersRouter);
router.use("/films", middlewares.checkToken, apiFilmsRouter);

module.exports = router;
