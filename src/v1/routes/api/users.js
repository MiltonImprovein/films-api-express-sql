const config = require("../../../config");
const router = require("express").Router();
const bcrypt = require("bcryptjs");
const { User } = require("../../../db");
const { check, validationResult } = require("express-validator");
const moment = require("moment");
const jwt = require("jwt-simple");

router.post(
  "/register",
  [
    check("username", "Nombre de usuario es requerido").not().isEmpty(),
    check("password", "Password es requerida").not().isEmpty(),
    check("email", "Email debe tener un formato correcto").isEmail(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    req.body.password = bcrypt.hashSync(req.body.password, 10);
    const user = await User.create(req.body);
    res.status(200).json(user);
  }
);
router.post("/login", async (req, res) => {
  const user = await User.findOne({ where: { email: req.body.email } });
  if (!user) {
    res.status(401).json({ error: "Error, verifica tus credenciales" });
  } else {
    const equals = bcrypt.compareSync(req.body.password, user.password);
    if (!equals) {
      res.status(401).json({ error: "Error, verifica tu password" });
    } else {
      res.status(200).json({ success: createToken(user) });
    }
  }
});
const createToken = (user) => {
  const payload = {
    userId: user.id,
    createdAt: moment().unix(),
    expiredAt: moment().add(5, "days").unix(),
  };
  return jwt.encode(payload, config.secretPhrase);
};
module.exports = router;
