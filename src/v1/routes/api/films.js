const router = require("express").Router();
const { Film } = require("../../../db");

router.get("/", async (req, res) => {
  const films = await Film.findAll();
  res.status(200).json(films);
});

router.post("/", async (req, res) => {
  const film = await Film.create(req.body);
  res.status(200).json(film);
});

router.patch("/:filmId", async (req, res) => {
  await Film.update(req.body, {
    where: { id: req.params.filmId },
  });
  res.status(200).json({ success: "Registro actualizado" });
});

router.delete("/:filmId", async (req, res) => {
  await Film.destroy({
    where: { id: req.params.filmId },
  });
  res.status(200).json({ success: "Registro eliminado" });
});

module.exports = router;
