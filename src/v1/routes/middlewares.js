const config = require("../../config");
const jwt = require("jwt-simple");
const moment = require("moment");

const checkToken = (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).json({ error: "No credentials sent!" });
  }
  const userToken = req.headers.authorization.split(" ")[1];
  let payload = {};
  try {
    payload = jwt.decode(userToken, config.secretPhrase);
  } catch (error) {
    return res.status(401).json({ error: "El token es incorrecto" });
  }
  if (payload.expiredAt < moment().unix()) {
    return res.status(401).json({ error: "El token a expirado" });
  }
  req.userId = payload.userId;
  next();
};

module.exports = {
  checkToken,
};
