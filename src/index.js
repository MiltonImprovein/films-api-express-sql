const config = require('./config')
const express = require("express");
const bodyParser = require("body-parser");
const apiRouterV1 = require("./v1/routes/api");
const app = express();
require("./db");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api/v1", apiRouterV1);

app.listen(config.port, () => {
  console.log("listening on port "+config.port);
});
