const config = require("./config");
const Sequelize = require("sequelize");
const FilmModel = require("./models/films");
const UserModel = require("./models/users");

const sequelize = new Sequelize(
  config.dbName,
  config.dbUser,
  config.dbPassword,
  {
    host: config.dbHost,
    dialect: "mysql",
  }
);

const Film = FilmModel(sequelize, Sequelize);
const User = UserModel(sequelize, Sequelize);

sequelize.sync().then(() => {
  console.log("Tables synced successfully");
});

module.exports = {
  Film,
  User,
};
