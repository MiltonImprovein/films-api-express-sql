require("dotenv").config();

const config = {
  dbHost: process.env.DB_HOST ?? "localhost",
  dbName: process.env.DB_NAME ?? "db_filmsapi",
  dbUser: process.env.DB_USER ?? "root",
  dbPassword: process.env.DB_PASSWORD ?? "asdasd",
  port: process.env.PORT ?? "3000",
  secretPhrase: process.env.SECRET_PHRASE ?? "frase secreta",
};
module.exports = config;
